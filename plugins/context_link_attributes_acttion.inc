<?php
/**
 * Expose http header changes as context reactions.
 */
class context_link_attributes_action extends context_reaction {

  function options_form($context) {
    $values = $this->fetch_from_context($context);
    $form['pattern'] = array(
      '#type' => 'textarea',
      '#rows' => 5,
      '#title' => t('Parttern'),
      '#default_value' => isset($values['pattern']) ? $values['pattern'] : '',
    );
    $form['attr'] = array(
      '#title' => 'attr',
      '#type' => 'textfield',
      '#default_value' => isset($values['attr']) ? $values['attr'] : '',
    );
    $form['value'] = array(
      '#title' => 'value',
      '#type' => 'textfield',
      '#default_value' => isset($values['value']) ? $values['value'] : '',
    );
    return $form;
  } 
  /**
   * Public method that is called from hooks or other integration points.
   */
  function execute(&$vars) {
    $contexts = $this->get_contexts();
    
    foreach ($contexts as $context) {
      $attr = &$vars['options']['attributes'];
      $pattern = $items = explode("\n", $context->reactions[$this->plugin]['pattern']);
      if (!empty($pattern)) {
        foreach ($pattern as $v) {
          $v = trim($v);
          if (!empty($v)) {
            $paths[$v] = $v;
          }
        }
      }
      $current_path = array(drupal_get_path_alias($vars['path']));
      if ($this->match($current_path, $paths, TRUE)) {
        $value = array($context->reactions[$this->plugin]['attr'] => $context->reactions[$this->plugin]['value']);
        $attr = array_merge($value, $attr);
      }

    }
  }
  //Copy from context_condition_path.inc
  //http://cgit.drupalcode.org/context/tree/plugins/context_condition_path.inc#n62
  protected function match($subject, $patterns, $path = FALSE) {
    static $regexps;
    $match = FALSE;
    $positives = $negatives = 0;
    $subject = !is_array($subject) ? array($subject) : $subject;
    foreach ($patterns as $pattern) {
      if (strpos($pattern, '~') === 0) {
        $negate = TRUE;
        $negatives++;
      }
      else {
        $negate = FALSE;
        $positives++;
      }
      $pattern = ltrim($pattern, '~');
      if (!isset($regexps[$pattern])) {
        if ($path) {
          $regexps[$pattern] = '/^(' . preg_replace(array('/(\r\n?|\n)/', '/\\\\\*/', '/(^|\|)\\\\<front\\\\>($|\|)/'), array('|', '.*', '\1' . preg_quote(variable_get('site_frontpage', 'node'), '/') . '\2'), preg_quote($pattern, '/')) . ')$/';
        }
        else {
          $regexps[$pattern] = '/^(' . preg_replace(array('/(\r\n?|\n)/', '/\\\\\*/'), array('|', '.*'), preg_quote($pattern, '/')) . ')$/';
        }
      }
      foreach ($subject as $value) {
        if (preg_match($regexps[$pattern], $value)) {
          if ($negate) {
            return FALSE;
          }
          $match = TRUE;
        }
      }
    }
    // If there are **only** negative conditions and we've gotten this far none
    // we actually have a match.
    if ($positives === 0 && $negatives) {
      return TRUE;
    }
    return $match;
  }
}